import React, { Component } from 'react';
import AdvocatesContainer from './components/AdvocatesContainer';
import './App.css';

class App extends Component {
	render() {
		return <AdvocatesContainer />
	}
}

export default App;