import React, { Component } from 'react';
import AboutModal from '../components/AboutModal';
import ContributeModal from '../components/ContributeModal';

class Advocates extends Component{
  constructor(props){
    super(props);
    this.state = {
      contributeShow: false, // Set the contribute box to not show
      aboutShow: false, // Set the about box to not show
      limit: 12 // Set the limit to number of results we want to show
    }
  }

  // Handle the states for showing the contribute box
  _handleOpenContribute() {
    this.state.contributeShow === false ? this.setState({ contributeShow: true }) : this.setState({ contributeShow: false })
  }

  // Handle the states for showing the about box
  _handleOpenAbout() {
    this.state.aboutShow === false ? this.setState({ aboutShow: true }) : this.setState({ aboutShow: false }) 
  }

  // Handle the load more button. When clicked, add 12 to the current limit, showing 12 results at a time
  _handleLoadMore() {
    this.setState({
      limit: this.state.limit+12
    })
  }

  render() {
    // Set self for use in the while loop later
    let self = this

    // Set up an identified to hold the advocates array
    // Provide two filters for first name and surname, and gender
    // Then profive a map function to build the array
    // Inside this we have a while loop which shows everything within the defined limit set in the state
    const advocates = this.props.advocates
    .filter(advocate => {
      return advocate.title.toLowerCase().indexOf(this.props.filterTerm.toLowerCase()) >= 0 
      || advocate.field_quote.toLowerCase().indexOf(this.props.filterTerm.toLowerCase()) >= 0 
    })
    .filter(advocate => {
      if(this.props.gender !== null){
        return advocate.field_gender === this.props.gender
      } else {
        return true
      }
    })
    .map(function(advocate, index){
      while(index < self.state.limit){
        return (
          <div key={index} className="col-sm-12 col-md-6 col-xl-4 mb-4">
            <div className="card h-100">
              <div className="card-image" style={{backgroundImage:'url(http://ubibackers.com' + advocate.field_picture + ')'}}></div>
              <div className="card-body">
                <h4 className="card-title" dangerouslySetInnerHTML={{__html: advocate.title}}></h4>
                <p className="card-text"><small dangerouslySetInnerHTML={{__html: advocate.field_title}} className="text-muted"></small></p>
                <p className="card-text" dangerouslySetInnerHTML={{__html: advocate.field_quote}}></p>
              </div>
              <div className="card-footer">
                <small className="text-muted"><a target="_blank" href={advocate.field_source} className="card-link">Source</a></small>
              </div>
            </div>
          </div>
        )
      }
      return true
    })

    return(
      <div className="container p-5">
        <h1 className="mb-3">UBI Backers</h1>
        <p className="mb-4">A curated list of notable people backing the idea of a universal basic income. We currently have <span className="badge badge-primary">{this.props.advocates.length}</span> backers listed!</p>

        <div className="addthis_inline_share_toolbox"></div>

        <ul className="links">
          <li onClick={this._handleOpenContribute.bind(this)} className="btn btn-primary btn-sm">Contribute</li>
          <li onClick={this._handleOpenAbout.bind(this)} className="btn btn-secondary btn-sm">About</li>
        </ul>

        <div className="row">
          <div className="col-sm-12 col-md-9">
            <SearchInput goSearch={this.props.filter} term={this.props.filterTerm} />
          </div>
          <div className="col-sm-12 col-md-3">
            <GenderSelect goGender={this.props.genderSet} />
          </div>
        </div>
        
        <div className="row">
          {advocates}

          {
            advocates.length < 1 &&
            <div className="col-sm-12 text-center mt-5">
              <h2>Nobody found!</h2>
              <p>Try searching again</p>
            </div>
          }
        </div>

        {
          advocates.length > this.state.limit &&
          <div className="text-center">
            <button className="btn btn-primary" onClick={this._handleLoadMore.bind(this)}>Load More</button>
          </div>
        }

        {
          this.state.contributeShow &&
          <ContributeModal closeModal={this._handleOpenContribute.bind(this)} />
        }

        {
          this.state.aboutShow &&
          <AboutModal closeModal={this._handleOpenAbout.bind(this)} />
        }

      </div>
    )
  }
}

class SearchInput extends Component {
  render(){
    return <input className="form-control mb-4" type="text" value={this.props.term} placeholder="Search for a person or quote" autoFocus onChange={this.props.goSearch} />
  }
}

class GenderSelect extends Component {
  render() {
    return (
      <select className="custom-select form-control mb-4" onChange={this.props.goGender}>
        <option value="">All</option>
        <option value="Man">Men</option>
        <option value="Woman">Women</option>
        <option value="Other">Other</option>
      </select>
    )
  }
}

export default Advocates;