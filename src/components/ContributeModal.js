import React, { Component } from 'react';

class ContributeModal extends Component {
  render() {
    return (
      <div className="modal" style={{display:"block"}}>
        <div className="mask"></div>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Help us out!</h5>
              <button onClick={this.props.closeModal} type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>Thanks for your interest in contributing toward this project! To get started you'll need to create an account. Click the button below to register in seconds. Once you're in you can add and edit UBI advocates as you please.</p>
              <p>This is a non-profit project for people who want to spread the UBI message. So play nice... or else.</p>
            </div>
            <div className="modal-footer">
              <a href="/api/user/register" className="btn btn-primary">Create account</a>
              <button onClick={this.props.closeModal} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ContributeModal;