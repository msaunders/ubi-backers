import React, { Component } from 'react';
import Advocates from '../components/Advocates';

class AdvocatesContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      filterTerm: "",
      gender: null,
      advocates: [
        {
          "title":"UBI Advocate",
          "field_quote":"UBI is great!",
          "field_source":"http://basicincome.org",
          "field_picture":"/api/sites/default/files/styles/large/public/default_images/default-profile-bfeeabd02c3b38305b18e4c2345fd54dbbd1a0a7bf403a31f08fca4fada50449.png",
          "field_title":"A progressive organisation"
        }
      ]
    }
  }

  componentDidMount(){
    let self = this;
    fetch('http://ubibackers.com/api/advocates')
    .then(function(response) {
      if (response.status >= 400) {
        alert("Looks like there is a problem with the API. Check back later!")
      } else {
        return response.json()
      }
    })
    .then(function(data) {
      self.setState({
        advocates: data
      })
    })
  }

  _handleFilter = (e) => {
    this.setState({
      filterTerm: e.target.value
    })
  }

  _handleGender = (e) => {
    if(e.target.value === ""){
      this.setState({
        gender: null
      })
    } else {
      this.setState({
        gender: e.target.value
      })
    }
  }

  render() {
    return <Advocates 
      advocates={this.state.advocates} 
      filter={this._handleFilter} 
      filterTerm={this.state.filterTerm} 
      gender={this.state.gender} 
      genderSet={this._handleGender} />
  }
}

export default AdvocatesContainer;