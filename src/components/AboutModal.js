import React, { Component } from 'react';

class AboutModal extends Component {
  render() {
    return (
      <div className="modal" style={{display:"block"}}>
        <div className="mask"></div>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">About this project</h5>
              <button onClick={this.props.closeModal} type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>The goal of this project is to provide a resource for bloggers, journalists and basic income fans to quickly find information on influential UBI advocates.</p>
              <p>It was created by <a href="http://twitter.com/matt5409">@matt5409</a> using:</p>
              <ul>
                <li>Bootstrap 4</li>
                <li>Drupal 8 (providing an open <a href="http://ubibackers.com/api/advocates">API here</a>)</li>
                <li>ReactJs</li>
              </ul>
              <p>You can fork a copy of this project here on <a href="https://bitbucket.org/msaunders/ubi-backers">Bitbucket</a>.</p>
            </div>
            <div className="modal-footer">
              <button onClick={this.props.closeModal} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default AboutModal;